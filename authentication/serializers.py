import re
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers
from authentication.models import Account
from django.contrib.auth import update_session_auth_hash

from authentication.models import PasswordResetRequest


class AccountSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=False)
    confirm_password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = Account
        fields = ('id', 'email', 'username', 'phone', 'created_at', 'updated_at', 'first_name',
                  'last_name', 'password', 'confirm_password', 'activation_code',)
        read_only_fields = ('created_at', 'updated_at', 'activation_code')

    def validate(self, attrs):
        password = attrs.get('password', None)
        confirm_password = attrs.get('confirm_password', None)

        if self.context.get('request').method == 'POST' and (not password or not confirm_password):
            raise serializers.ValidationError({
                'password': 'Password is required.'
            })

        if password and confirm_password:
            if not password == confirm_password:
                raise serializers.ValidationError({
                    'password': 'Password and Confirm Password do not match.'
                })

            if len(password) < 6:
                raise serializers.ValidationError({
                    'password': 'Password must be at least 6 characters long.'
                })

        return attrs

    def validate_email(self, value):
        pattern = re.compile(r'[a-zA-Z0-9\._-]+@spjimr.org$')
        if pattern.match(value):
            return value
        raise serializers.ValidationError('Email should be of the format john.doe@spjimr.org.')

    def create(self, validated_data):
        return Account.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.save()

        password = validated_data.get('password', None)
        confirm_password = validated_data.get('confirm_password', None)

        if password and confirm_password and password == confirm_password:
            instance.set_password(password)
            instance.save()
            update_session_auth_hash(self.context.get('request'), instance)

        return instance


class PasswordResetRequestSerializer(serializers.ModelSerializer):
    user = serializers.EmailField()

    class Meta:
        model = PasswordResetRequest
        fields = ('uuid', 'created_at', 'user')

    def validate(self, attrs):
        try:
            Account.objects.get(email=attrs.get('user'))
        except ObjectDoesNotExist:
            raise serializers.ValidationError({
                'email': 'Invalid email id or request invalid.'
            })

        return attrs