from rest_framework import permissions


class IsAccountOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Write permissions are only allowed to the owner of the Account
        if request.user:
            return obj == request.user
        return False