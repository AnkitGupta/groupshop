import json
from datetime import datetime, timedelta
from django.utils.timezone import utc

from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

from rest_framework import viewsets, views, generics, permissions, status
from rest_framework.response import Response

from django.contrib.auth import authenticate, login, logout

from authentication.serializers import AccountSerializer, PasswordResetRequestSerializer
from authentication.models import Account, PasswordResetRequest
from authentication.permissions import IsAccountOwner

from utils.mailer import send_mail, get_or_create_template


class AccountViewSet(viewsets.ModelViewSet):
    lookup_field = 'username'
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return [permissions.IsAuthenticated()]

        if self.request.method == 'POST':
            return [permissions.AllowAny()]

        return [permissions.IsAuthenticated(), IsAccountOwner()]

    def get_link(self, code):
        return self.request.build_absolute_uri(reverse('account_activation').
                                               replace('api', '#')) + code

    def perform_create(self, serializer):
        serializer.save()
        send_mail(
            to_email=serializer.data.get('email'),
            template=get_or_create_template(
                name='account_register',
                subject='GroupShop Team :: Account activation',
                path='email/account/register',
            ),
            context={
                'link': self.get_link(serializer.data.get('activation_code')),
                'username': serializer.data.get('username') or ''
            }
        )


class AccountActivationView(views.APIView):
    def get(self, request, format=None):
        code = request.query_params.get('code') or None
        try:
            account = Account.objects.get(activation_code=code)
        except ObjectDoesNotExist:
            return Response({
                'message': 'Invalid activation code'
            }, status=status.HTTP_400_BAD_REQUEST)

        if not account.is_active:
            account.is_active = True
            account.save()

        return Response({
            'message': 'Account verification successful.'
        })


class LoginView(views.APIView):
    def post(self, request, format=None):
        data = json.loads(request.body)

        email = data.get('email', None)
        password = data.get('password', None)

        account = authenticate(email=email, password=password)

        if account is not None:
            if account.is_active:
                login(request, account)

                serializer = AccountSerializer(account, context={'request': request})
                return Response(serializer.data)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled or not been activated'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'Login failed! Email/Password combination incorrect.'
            }, status=status.HTTP_401_UNAUTHORIZED)


class LogoutView(views.APIView):
    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, format=None):
        logout(request)
        return Response({}, status=status.HTTP_204_NO_CONTENT)


class PasswordResetRequestView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def get_link(self, uuid):
        return self.request.build_absolute_uri(reverse('password_reset').
                                               replace('api', '#')) + uuid

    def post(self, request, format=None):
        serializer = PasswordResetRequestSerializer(data=request.data)
        if serializer.is_valid():
            user = Account.objects.get(email=request.data.get('user'))
            serializer.save(user=user)
            send_mail(
                to_email=user.email,
                template=get_or_create_template(
                    name='password_reset_request',
                    subject='GroupShop Team :: Password Reset Request',
                    path='email/password/reset_request',
                ),
                context={
                    'link': self.get_link(serializer.data.get('uuid')),
                    'username': serializer.data.get('username') or ''
                }
            )
            return Response(None, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordResetView(views.APIView):
    permission_classes = [permissions.AllowAny]

    def get(self, request, format=None):
        uuid = request.query_params.get('id', None)
        try:
            reset_request = PasswordResetRequest.objects.get(uuid=uuid)
        except ObjectDoesNotExist:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

        delta = datetime.utcnow().replace(tzinfo=utc) - reset_request.created_at
        if delta.days >= 1:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

        return Response(None, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        data = json.loads(request.body)

        uuid = request.query_params.get('id', None)
        try:
            reset_request = PasswordResetRequest.objects.get(uuid=uuid)
        except ObjectDoesNotExist:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

        delta = datetime.utcnow().replace(tzinfo=utc) - reset_request.created_at
        if delta.days >= 1:
            return Response(None, status=status.HTTP_404_NOT_FOUND)

        password = data.get('password', None)
        confirm_password = data.get('confirm_password', None)

        if not password or not confirm_password:
            return Response({
                'password': ['Password and Confirm Password is required']
            }, status=status.HTTP_400_BAD_REQUEST)

        if not password == confirm_password:
            return Response({
                'password': ['Password and Confirm Password do not match']
            }, status=status.HTTP_400_BAD_REQUEST)

        if len(password) < 6:
            return Response({
                'password': ['Password should be at least 6 characters long.']
            }, status=status.HTTP_400_BAD_REQUEST)

        reset_request.user.set_password(password)
        reset_request.user.save()
        # invalidate reset request
        reset_request.created_at = reset_request.created_at - timedelta(days=1)
        reset_request.save()

        return Response({}, status=status.HTTP_200_OK)

