# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0008_account_activation_code_01'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='activation_code',
            field=models.CharField(default=uuid.uuid4, unique=True, max_length=100, blank=True),
        ),
    ]
