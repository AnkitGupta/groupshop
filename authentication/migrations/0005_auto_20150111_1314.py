# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0004_passwordreset'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PasswordReset',
            new_name='PasswordResetRequest',
        ),
    ]
