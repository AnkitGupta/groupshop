# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0006_auto_20150112_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='activation_code',
            field=models.CharField(max_length=100, null=True),
            preserve_default=True,
        ),
    ]
