# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import uuid


def propagate_unique_activation_code(apps, schema_editor):
    Account = apps.get_model('authentication', 'Account')
    db_alias = schema_editor.connection.alias
    for e in Account.objects.using(db_alias):
        e.activation_code=uuid.uuid4()
        e.is_active = True
        e.save()


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0007_account_activation_code'),
    ]

    operations = [
        migrations.RunPython(
            propagate_unique_activation_code,
        ),
    ]
