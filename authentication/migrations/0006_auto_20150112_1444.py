# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0005_auto_20150111_1314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='passwordresetrequest',
            name='user',
            field=models.ForeignKey(related_name=b'requests', to=settings.AUTH_USER_MODEL),
        ),
    ]
