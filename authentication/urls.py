from rest_framework import routers
from django.conf.urls import patterns, url, include
from authentication.views import AccountViewSet, LoginView, LogoutView, \
    PasswordResetRequestView, PasswordResetView, AccountActivationView


router = routers.DefaultRouter()
router.register(r'accounts', AccountViewSet)


urlpatterns = patterns('',
    url(r'^accounts/login/$', LoginView.as_view(), name='login'),
    url(r'^accounts/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^accounts/password/reset/request/$', PasswordResetRequestView.as_view(), name='password_reset_request'),
    url(r'^accounts/password/reset/$', PasswordResetView.as_view(), name='password_reset'),
    url(r'^accounts/activation/$', AccountActivationView.as_view(), name='account_activation'),
    url(r'^', include(router.urls)),
)