(function() {
    'use strict';

    angular.module('groupshop.utils.services').

    factory('Snackbar', function() {
        var Snackbar = {
            error: error,
            show: show
        };

        return Snackbar;

        function _snackbar(content, options) {
            options = angular.extend({timeout: 5000}, options);
            options.content = content;

            $.snackbar(options);
        }

        function error(content, options) {
            if (undefined == content) return;
            _snackbar('Error: ' + content, options);
        }

        function show(content, options) {
            if (undefined == content) return;
            _snackbar(content, options);
        }
    })

})();