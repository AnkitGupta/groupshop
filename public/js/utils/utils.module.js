(function() {
    'use strict';

    angular.module('groupshop.utils', [
        'groupshop.utils.services',
        'groupshop.utils.filters'
    ]);

    angular.module('groupshop.utils.services', []);
    angular.module('groupshop.utils.filters', []);
})();