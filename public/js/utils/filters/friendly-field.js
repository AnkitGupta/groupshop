(function() {
    angular.module('groupshop.utils.filters').
    filter('friendlyField', function() {
        return function(s) {
            return s.replace('_', ' ').split(' ').
                    map(function(word) { return word[0].toUpperCase() + word.slice(1) }).join(' ');
        }
    });
}) ();