(function() {
    'use strict';

    angular.module('groupshop.config').

    config([
        '$httpProvider',
        '$resourceProvider',
        '$locationProvider',

        function($httpProvider, $resourceProvider, $locationProvider, ngDialogProvider) {
            $resourceProvider.defaults.stripTrailingSlashes = false;

            $httpProvider.defaults.xsrfCookieName = 'csrftoken';
            $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        }
    ]);
})();