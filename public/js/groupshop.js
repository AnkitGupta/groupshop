(function() {
    'use strict';

    angular.module('groupshop', [
        'ngResource',
        'groupshop.config',
        'groupshop.routes',
        'groupshop.layout',
        'groupshop.authentication',
        'groupshop.profiles',
        'groupshop.orders',
        'groupshop.products',
        'groupshop.utils'
    ]);

    angular.module('groupshop.config', []);
    angular.module('groupshop.routes', ['ui.router']);
})();



