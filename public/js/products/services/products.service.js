(function() {
    'use strict';
    angular.module('groupshop.products.services').

    factory('Product', ['$resource', function($resource) {
        return $resource('/api/products/:id', {id: '@id'}, {
           update: {
               method: 'PUT'
           }
        });
    }]).

    factory('OrderProduct', ['$resource', function($resource) {
       return $resource('/api/orders/:order_id/products');
    }]);

})();