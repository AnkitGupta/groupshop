(function() {
    'use strict';

    angular.module('groupshop.products.directives').

    directive('productGroupForm', function(){
        return {
            restrict: 'E',
            scope: {
                products: '='
            },
            templateUrl: 'directives/products/product-group.form.html',
            controller: 'ProductGroupFormCtrl'
        };
    }).

    directive('productForm', function() {
        return {
            restrict: 'E',
            scope: {
                product: '='
            },
            templateUrl: 'directives/products/product.form.html'
        };
    })
})();