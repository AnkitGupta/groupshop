(function() {
    'use strict';

    angular.module('groupshop.products.controllers').

    controller('ProductGroupFormCtrl', ['$scope', 'Product', function($scope, Product) {
        $scope.addProduct = function() {
            $scope.products.push(new Product());
        };

        $scope.removeProduct = function(product) {
            $scope.products.pop();
        };
    }]);
})();