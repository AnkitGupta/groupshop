(function() {
    'use strict';

    angular.module('groupshop.products', [
        'groupshop.products.controllers',
        'groupshop.products.directives',
        'groupshop.products.services'
    ]);

    angular.module('groupshop.products.controllers', []);
    angular.module('groupshop.products.directives', []);
    angular.module('groupshop.products.services', []);
})();