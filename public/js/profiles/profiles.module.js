(function() {
    'use strict';

    angular.module('groupshop.profiles', [
        'groupshop.profiles.controllers',
        'groupshop.profiles.services'
    ]);

    angular.module('groupshop.profiles.controllers', []);
    angular.module('groupshop.profiles.services', []);
})();