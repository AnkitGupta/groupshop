(function() {
    'use strict';

    angular.module('groupshop.profiles.services').
    factory('Profile', Profile);

    Profile.$inject = ['$resource'];

    function Profile($resource) {
        return $resource('/api/accounts/:username', {'username': '@username'}, {
            update: {
                method: 'PUT'
            }
        });
    }
})();