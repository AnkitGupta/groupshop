(function() {
    'use strict';

    angular.module('groupshop.profiles.controllers').
    controller('ProfileSettingsCtrl', ProfileSettingsCtrl);

    ProfileSettingsCtrl.$inject = ['$scope', '$state', '$stateParams', 'Authentication', 'Profile', 'Snackbar'];

    function ProfileSettingsCtrl($scope, $state, $stateParams, Authentication, Profile, Snackbar) {
        $scope.update = update;

        function update() {
            $scope.isSaving = true;
            $scope.profile.$update().then(profileSuccessFn, profileErrorFn);

            function profileSuccessFn(data) {
                Snackbar.show('Your profile has been updated');
                $scope.confirm();
            }

            function profileErrorFn(data) {
                $scope.isSaving = false;
                $scope.errors = data.data;
            }
        }
    }
}) ();