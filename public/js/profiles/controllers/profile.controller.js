(function() {
    'use strict';

    angular.module('groupshop.profiles.controllers').
    controller('ProfileCtrl', ProfileCtrl);

    ProfileCtrl.$inject = [
        '$scope', 'Profile', 'Authentication', 'ngDialog', 'AccountOrder', '$state', '$stateParams', 'Snackbar'
    ];

    function ProfileCtrl($scope, Profile, Authentication, ngDialog, AccountOrder, $state, $stateParams, Snackbar) {
        $scope.profile = undefined;
        $scope.edit = edit;
        $scope.isOwner = false;
        $scope.orders = [];

        activate();

        function activate() {
            var username = $stateParams.username;
            var authenticatedAccount = Authentication.getAuthenticatedAccount();

            if (authenticatedAccount && authenticatedAccount.username === username) {
                $scope.isOwner = true;
            }

            $scope.profile = Profile.get({ username: username });
            $scope.profile.$promise.then(null, profileErrorFn);

            $scope.orders = AccountOrder.query({ username: username, status: 3 });
            $scope.orders.$promise.then(null, accountOrderErrorFn);

            function profileErrorFn() {
                $state.go('home');
                Snackbar.error('User does not exist');
            }

            function accountOrderErrorFn(data) {
                Snackbar.error(data.error);
            }
        }

        function edit() {
            ngDialog.openConfirm({
                template: 'partials/profiles/settings.html',
                controller: 'ProfileSettingsCtrl',
                className: 'ngdialog-theme-plain',
                scope: $scope
            });
        }
    }
})();