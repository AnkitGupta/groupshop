(function() {
    'use strict';

    angular.module('groupshop.authentication', [
        'groupshop.authentication.controllers',
        'groupshop.authentication.services',
        'groupshop.authentication.directives'
    ]);

    angular.module('groupshop.authentication.services', ['ngCookies']);
    angular.module('groupshop.authentication.controllers', []);
    angular.module('groupshop.authentication.directives', []);
})();