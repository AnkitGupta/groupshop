(function() {
    'use strict';

    angular.module('groupshop.authentication.controllers').
    controller('RegisterCtrl', RegisterCtrl);

    RegisterCtrl.$inject = ['$scope', 'Authentication', 'Account', 'Snackbar'];

    function RegisterCtrl($scope, Authentication, Account, Snackbar) {
        activate();

        function activate() {
            $scope.account = new Account();
            $scope.register = register;

            function register() {
                $scope.errors = null;
                if (!angular.equals($scope.account.password, $scope.account.confirm_password)) {
                    Snackbar.error('Passwords do not match!');
                    return;
                }
                $scope.isSaving = true;
                Authentication.register($scope.account).then(null, function(errors) {
                    $scope.isSaving = false;
                    $scope.errors = errors;
                });
           }
        }
    }
})();