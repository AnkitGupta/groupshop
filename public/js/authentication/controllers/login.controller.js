(function() {
    'use strict';

    angular.module('groupshop.authentication.controllers').
    controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$scope', '$state', 'Authentication', 'ngDialog'];

    function LoginCtrl($scope, $state, Authentication, ngDialog) {
        activate();

        function activate() {
            $scope.login = login;
            $scope.forgotPassword = forgotPasswordDialog;
        }

        function login() {
            $scope.isSaving = true;
            Authentication.login($scope.email, $scope.password).then(function () {
                $state.go('orders');
            }, function () {
                $scope.isSaving = false;
            });
        }

        function forgotPasswordDialog() {
            ngDialog.openConfirm({
                template: 'partials/authentication/forgot-password.html',
                controller: 'ForgotPasswordCtrl',
                className: 'ngdialog-theme-plain'
            });
        }
    }
})();