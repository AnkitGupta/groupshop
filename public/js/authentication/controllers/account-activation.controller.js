(function() {
    angular.module('groupshop.authentication.controllers').
    controller('AccountActivationCtrl', AccountActivationCtrl);

    AccountActivationCtrl.$inject = ['$scope', '$http', '$stateParams'];

    function AccountActivationCtrl($scope, $http, $stateParams) {
        var code = $stateParams.id;
        activate();

        function activate() {
            $http.get('/api/accounts/activation/', {
                params: {
                    'code': code
                }
            }).then(activationSuccessFn, activationErrorFn);

            function activationSuccessFn() {
                $scope.is_active = true;
                $scope.message = 'Account has been verified successfully.';
            }

            function activationErrorFn() {
                $scope.message = 'Account verification failed. ' +
                    'Please check the activation link and try again.';
            }
        }
    }
})();