(function() {
    'use strict';

    angular.module('groupshop.authentication.controllers').
    controller('PasswordResetCtrl', PasswordResetCtrl);

    PasswordResetCtrl.$inject = ['$scope', '$state', '$stateParams', '$http', 'Snackbar'];

    function PasswordResetCtrl($scope, $state, $stateParams, $http, Snackbar) {
        activate();

        function activate() {
            $http.get('/api/accounts/password/reset/', {
                params: {
                    id: $stateParams.id
                }
            }).then(passwordResetValidSuccessFn, passwordResetValidErrorFn);

            function passwordResetValidSuccessFn() {
                $scope.reset = passwordReset;

                function passwordReset() {
                    $scope.isSaving = true;
                    $http.post('/api/accounts/password/reset/', {
                        'password': $scope.password,
                        'confirm_password': $scope.confirm_password
                    }, {
                        params: {
                            id: $stateParams.id
                        }
                    }).then(passwordResetSuccessFn, passwordResetErrorFn);

                    function passwordResetSuccessFn() {
                        Snackbar.show('Password reset successfully. Please login');
                        $state.go('home');
                    }

                    function passwordResetErrorFn(data) {
                        $scope.isSaving = false;
                        $scope.errors = data.data;
                    }
                }
            }

            function passwordResetValidErrorFn() {
                Snackbar.error('Password reset link invalid or expired.');
                $state.go('home');
            }
        }
    }
}) ();