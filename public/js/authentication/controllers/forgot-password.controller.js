(function() {
    'use strict';

    angular.module('groupshop.authentication.controllers').
    controller('ForgotPasswordCtrl', ForgotPasswordCtrl);

    ForgotPasswordCtrl.$inject = ['$scope', '$http', 'Snackbar'];

    function ForgotPasswordCtrl($scope, $http, Snackbar) {
        activate();

        function activate() {
            $scope.reset = passwordResetRequest;

            function passwordResetRequest() {
                $scope.isSaving = true;
                $http.post('/api/accounts/password/reset/request/', {
                    'user': $scope.email
                }).then(passwordResetRequestSuccessFn, passwordResetRequestErrorFn);

                function passwordResetRequestSuccessFn(data) {
                    Snackbar.show('Reset link sent to email: ' + $scope.email);
                    $scope.confirm();
                }

                function passwordResetRequestErrorFn(data) {
                    $scope.isSaving = false;
                    $scope.errors = data.data;
                }
            }
        }
    }
}) ();