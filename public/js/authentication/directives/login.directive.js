(function() {
    'use strict';

    angular.module('groupshop.authentication.directives').
    directive('login', login);

    function login() {
        var directive = {
            restrict: 'E',
            scope: {},
            controller: 'LoginCtrl',
            templateUrl: 'directives/authentication/login.html'
        };

        return directive;
    }
})();