(function(){
    'use strict';

    angular.module('groupshop.authentication.services').

    factory('Account', ['$resource', function($resource) {
        return $resource('/api/accounts/:username', {'username': '@username'}, {
            save: {
                method: 'POST',
                params: {'username': ''}

            },
            update: {
                method: 'PUT'
            }
        });
    }]).

    factory('Authentication', [
        '$cookies',
        '$q',
        '$http',
        '$state',
        'Account',
        'Snackbar',

        function($cookies, $q, $http, $state, Account, Snackbar) {
            function register(account) {
                return account.$save().then(registerSuccessFn, registerErrorFn);

                function registerSuccessFn(data, status, headers, config) {
                    Snackbar.show('Account created successfully! Please check your email for activation link.');
                    $state.go('login');
                }

                function registerErrorFn(data, status, headers, config) {
                    Snackbar.error(data.data.message);
                    return $q.reject(data.data);
                }
            }

            function login(email, password) {
                return $http.post('/api/accounts/login/', {
                    email: email,
                    password: password
                }).then(loginSuccessFn, loginErrorFn);

                function loginSuccessFn(data, status, headers, config) {
                    Snackbar.show('Logged in successfully!');
                    setAuthenticatedAccount(data.data);
                    $state.go('home');
                }

                function loginErrorFn(data, status, headers, config) {
                    Snackbar.error(data.data.message);
                    return $q.reject(data.data);
                }
            }

            function logout() {
                return $http.post('/api/accounts/logout/', {}).then(logoutSuccessFn, logoutErrorFn);

                function logoutSuccessFn(data, status, headers, config) {
                    Snackbar.show('Logged out successfully!');
                    unAuthenticate();
                    $state.go('home');
                }

                function logoutErrorFn(data, status, headers, config) {
                    Snackbar.error(data.error);
                }
            }

            function getAuthenticatedAccount() {
                if (!$cookies.authenticatedAccount) {
                    return;
                }

                return JSON.parse($cookies.authenticatedAccount)
            }

            function isAuthenticated() {
                return !!$cookies.authenticatedAccount;
            }

            function setAuthenticatedAccount(account) {
                $cookies.authenticatedAccount = JSON.stringify(account);
            }

            function unAuthenticate() {
                delete $cookies.authenticatedAccount;
            }

            return {
                'register': register,
                'login': login,
                'logout': logout,
                'getAuthenticatedAccount': getAuthenticatedAccount,
                'setAuthenticatedAccount': setAuthenticatedAccount,
                'isAuthenticated': isAuthenticated,
                'unAuthenticated': unAuthenticate
            };
        }
    ]);
})();