(function() {
    'use strict';

    angular.module('groupshop.orders.directives').
    directive('orders', orders);

    orders.$inject = ['$rootScope'];

    function orders($rootScope) {
        function link(scope, element, attrs) {
            $(window).scroll(function() {
                if ($(window).scrollTop() + $(window).height() >= $(document).height() - 300){
                    $rootScope.$broadcast('load-content');
                }
            });
        }

        var directive = {
            link: link,
            restrict: 'E',
            templateUrl: 'directives/orders/list.html',
            scope: {
                orders: '=',
                options: '='
            },
            controller: 'OrdersCtrl'
        };

        return directive;
    }
})();