(function() {
    'use strict';

    angular.module('groupshop.orders.directives').
    directive('orderNewBtn', orderNewBtn);

    orderNewBtn.$inject = [];

    function orderNewBtn() {
        var directive = {
            restrict: 'E',
            scope: {},
            template:   '<a ng-click="new()" class="btn btn-primary" title="Click here to buffer order of your products">' +
                        '   <span class="glyphicon glyphicon-plus"></span>&nbsp;Create Order' +
                        '</a>',
            controller: 'OrderNewBtnCtrl'
        };

        return directive;
    }


})();