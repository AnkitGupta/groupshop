(function() {
    angular.module('groupshop.orders.controllers').
    controller('OrderDetailCtrl', OrderDetailCtrl);

    OrderDetailCtrl.$inject = [
        '$scope',
        '$state',
        '$http',
        'Authentication',
        'ngDialog',
        '$stateParams',
        'Order',
        'OrderProduct',
        'Snackbar',
        'ORDER_CONSTANTS'
    ];

    function OrderDetailCtrl(
        $scope,
        $state,
        $http,
        Authentication,
        ngDialog,
        $stateParams,
        Order,
        OrderProduct,
        Snackbar,
        ORDER_CONSTANTS
    ) {
        $scope.statuses = ORDER_CONSTANTS.STATUSES.map(function(status) {
            return status.name;
        });

        var order_copy;
        var products_all_copy;

        $scope.order = Order.get({ id: $stateParams.id });
        $scope.order.$promise.then(function(order) {
            order_copy = angular.copy(order);
            // All products part of the Clubbed Order
            $scope.products_all = OrderProduct.query({ order_id: $scope.order.id, recursive: 'true' });
            $scope.products_all.$promise.then(function(products) {
                products_all_copy = angular.copy(products);
            });
            $scope.products = OrderProduct.query({ order_id: $scope.order.id });
        });

        $scope.edit = editOrder;
        $scope.delete = deleteOrder;
        $scope.isOwner = isOwner;
        $scope.placeOrder = placeOrder;

        function editOrder() {
            ngDialog.openConfirm({
                template: 'partials/orders/edit.html',
                controller: 'OrderEditCtrl',
                className: 'ngdialog-theme-plain',
                scope: $scope
            }).then(editConfirmFn, editCancelFn);

            function editConfirmFn() {
                $scope.products_all = OrderProduct.query({ order_id: $scope.order.id, recursive: 'true' });
                $scope.products_all.$promise.then(function(products) {
                    $scope.products_all = products.filter(function(product) {
                        return product.order.id != $scope.order.id;
                    });
                    $scope.products_all = $scope.products.concat($scope.products_all);
                });
            }

            function editCancelFn() {
                $scope.order = order_copy;
                $scope.products_all = products_all_copy;
            }
        }

        function deleteOrder() {
            var dialog = ngDialog.openConfirm({
                template: 'partials/orders/delete-confirm.html',
                className: 'ngdialog-theme-plain'
            });

            dialog.then(function() {
                $scope.isSaving = true;
                $scope.order.$delete().then(orderDeleteSuccessFn, orderDeleteErrorFn);
            });

            function orderDeleteSuccessFn(data) {
                Snackbar.show('Order deleted successfully!');
                $state.go('orders');
            }

            function orderDeleteErrorFn() {
                $scope.isSaving = false;
                Snackbar.error('Operation failed!');
            }
        }

        function isOwner() {
            return $scope.order.$resolved && $scope.order.owner.username == Authentication.getAuthenticatedAccount().username;
        }

        /**
         * Prepares post params for LocalBanya (add a product to cart)
         * @param product
         * @returns {{prod_id: *, weight_code: *, quantity: *}}
         */
        function buildPostParams(product) {
            /**
             * Extracts product-id and product-weight-code from url.
             * @param url
             * @returns {Array}
             */
            function extractProductInfo(url) {
                return url.replace('?isCampaign=true', '').substr(url.lastIndexOf('/')+1).split('-');
            }

            var info = extractProductInfo(product.link);

            return {
                prod_id: info[0],
                weight_code: info[1],
                quantity: product.quantity
            };
        }

        /**
         * Place order on localbanya
         */
        function placeOrder() {
            if ($scope.products_all.$resolved) {
                var products = $scope.products_all.map(function(product) {
                    return buildPostParams(product);
                });

                var url = '/api/orders/' + $scope.order.id  + '/export_localbanya/';
                $http.post(url, products).then(exportSuccessFn, exportErrorFn);

                function exportSuccessFn(data) {
                    $scope.order = data.data;
                    Snackbar.show('Successfully exported your products to localbanya cart.');
                }

                function exportErrorFn(data) {
                    Snackbar.error(data.error);
                }
            }
        }
    }
})();