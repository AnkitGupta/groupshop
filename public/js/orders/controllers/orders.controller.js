(function() {
    'use strict';

    angular.module('groupshop.orders.controllers').
    controller('OrdersCtrl', OrdersCtrl);

    OrdersCtrl.$inject = ['$scope'];

    function OrdersCtrl($scope) {
        $scope.statuses = [
            'OPEN',
            'IN PROCESS',
            'COMPLETED'
        ];
    }
})();