(function() {
    'use strict';

    angular.module('groupshop.orders.controllers').

    controller('OrdersIndexCtrl', [
        '$scope',
        '$state',
        'Authentication',
        'Order',
        'ngDialog',
        'Snackbar',
        '$cookies',

        function($scope, $state, Authentication, Order, ngDialog, Snackbar, $cookies) {
            if (!Authentication.isAuthenticated()) {
                Snackbar.error('Private content, only for registered users. Please login!');
                $state.go('home');
            }

            $scope.currentStep = parseInt($cookies.ordersHomeTour, 10) || 0;
            $scope.postStepCallback = function() {
                $cookies.ordersHomeTour = $scope.currentStep;
            };

            var page = 1, next = null;
            var loaded = {};
            $scope.orders = [];

            function loadOrders() {
                if (next !== null) {
                    loaded[next] = true;
                }
                Order.paginated_query({ page: page, status: 3 }, function(orders) {
                    var results = orders.results.map(function(order) {
                        return new Order(order);
                    });

                    $scope.orders = $scope.orders.concat(results);
                    page = page + 1;
                    next = orders.next;
                });
            }

            loadOrders();

            $scope.$on('load-content', function() {
                if (next !== null && !loaded[next]) {
                    loadOrders();
                }
            });

            $scope.$on('order.created', function(event, order) {
                $scope.orders.unshift(order);
                order.group_orders.forEach(function(group_order) {
                    $scope.orders.some(function(order, idx) {
                        if (order.id == group_order.id) {
                            $scope.orders.splice(idx, 1);
                            return true;
                        }
                        return false;
                    });
                });
            });

            $scope.$on('order.created.error', function() {
                $scope.orders.shift();
            });
        }
    ]);
})();