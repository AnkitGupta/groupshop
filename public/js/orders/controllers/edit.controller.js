(function() {
    'use strict';

    angular.module('groupshop.orders.controllers').
    controller('OrderEditCtrl', OrderEditCtrl);

    OrderEditCtrl.$inject = [
        '$scope',
        'Order',
        'OrderProduct',
        'Product',
        'Snackbar',
        'ORDER_CONSTANTS',
        '$cookies'
    ];

    function OrderEditCtrl($scope, Order, OrderProduct, Product, Snackbar, ORDER_CONSTANTS, $cookies) {
        $scope.name = 'Edit';
        $scope.outlets = ORDER_CONSTANTS.OUTLETS;
        $scope.statuses = ORDER_CONSTANTS.STATUSES;

        $scope.currentStep = parseInt($cookies.ordersNewTour, 10) || 0;
        $scope.postStepCallback = function() {
            $cookies.ordersNewTour = $scope.currentStep;
        };

        var is_modified = true;
        var prev_products = angular.copy($scope.products);
        var listener = $scope.$watchCollection('products', function(newValue, oldValue) {
            if (angular.equals(newValue, oldValue)) {
                return;
            }
            is_modified = false;
            listener();
        });

        $scope.abc = {};
        $scope.rel_orders = [];
        $scope.$watch('order.online_store', function(value) {
            if (undefined !== value) {
                $scope.rel_orders = Order.query({ 'online_store': value, 'status': 3 });
                $scope.rel_orders.$promise.then(function(orders) {
                    $scope.rel_orders = orders.filter(function(order) {
                        return order.id !== $scope.order.id;
                    });

                    $scope.rel_orders = $scope.rel_orders.concat($scope.order.group_orders);
                    $scope.abc.group_orders = $scope.order.group_orders.map(function(order) {
                        return order.id;
                    });
                });
            }
        });

        $scope.save = saveOrder;

        function saveOrder() {
            if ($scope.products.length == 0) {
                Snackbar.error('You cannot save an order without any products!');
                return;
            }

            $scope.isSaving = true;
            $scope.order.group_orders = $scope.abc.group_orders;
            $scope.order.$update().then(orderUpdateSuccessFn, orderUpdateErrorFn);

            function orderUpdateSuccessFn(order) {
                $scope.isSaving = false;
                $scope.order.total = 0;
                if (is_modified) {
                    $scope.products.forEach(function(product) {
                        Product.update({ id: product.id }, product);
                        $scope.order.total += product.price * product.quantity;
                    });
                } else {
                    prev_products.forEach(function(product) {
                        Product.delete({ id: product.id });
                    });

                    $scope.products.forEach(function(product) {
                        product.order = order.id;
                        delete product.id;
                        Product.save({}, new Product(product));
                        $scope.order.total += product.price * product.quantity;
                    });
                }

                $scope.order.group_orders.forEach(function(order) {
                    $scope.order.total += order.total;
                });

                $scope.confirm();
                Snackbar.show('Successfully saved order details!');
                $scope.errors  = null;
            }

            function orderUpdateErrorFn(data) {
                $scope.errors = data.data;
                Snackbar.error('Operation failed!');
            }
        }
    }
})();