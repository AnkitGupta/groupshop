(function() {
    'use strict';

    angular.module('groupshop.orders.controllers').
    controller('OrderNewCtrl', OrderNewCtrl);

    OrderNewCtrl.$inject = ['$scope', '$rootScope', 'Order', 'Snackbar', 'ORDER_CONSTANTS', '$cookies', '$http', 'Product'];

    function OrderNewCtrl($scope, $rootScope, Order, Snackbar, ORDER_CONSTANTS, $cookies, $http, Product) {
        $scope.name = 'Create';
        $scope.order = new Order();
        $scope.products = [];
        $scope.outlets = ORDER_CONSTANTS.OUTLETS;

        $scope.abc = {};
        $scope.order.group_orders = $scope.abc.group_orders = [];
        $scope.importDisabled = false;

        $scope.currentStep = parseInt($cookies.ordersNewTour, 10) || 0;
        $scope.postStepCallback = function() {
            $cookies.ordersNewTour = $scope.currentStep;
        };

        $scope.rel_orders = [];
        $scope.$watch('order.online_store', function (value) {
            if (undefined !== value) {
                window.open(ORDER_CONSTANTS.OUTLET_LINKS[value], '_blank');
                $scope.rel_orders = Order.query({ 'online_store': $scope.order.online_store, 'status': 3 });
            }
        });

        $scope.save = function () {
            if ($scope.products.length == 0) {
                Snackbar.error('You cannot create an order without any products!');
                return;
            }

            $scope.isSaving = true;
            $scope.order.group_orders = $scope.abc.group_orders;
            $scope.order.$save().then(function (order) {
                $scope.products.forEach(function (product) {
                    product.order = order.id;
                    order.total += product.price * product.quantity;
                    product.$save();
                });

                $rootScope.$broadcast('order.created', order);
                $scope.closeThisDialog('Done!');
                $scope.errors = null;
                Snackbar.show('Successfully created order!');
            }, function (data) {
                $scope.isSaving = false;
                $scope.errors = data.data;
                $rootScope.$broadcast('order.created.error');
                Snackbar.error('Failed to save order. Please try again!');
            });
        };

        function importCart() {
            $scope.isSaving = true;
            Snackbar.show('Please wait while the import is being performed.');
            $http.get('/api/orders/import_localbanya').then(importSuccessFn, importErrorFn);

            function importSuccessFn(data) {
                var products = data.data;
                $scope.products = $scope.products.concat(
                    products.map(function (product) {
                        return new Product(product)
                    })
                );
                Snackbar.show('Successfully imported products from your Localbanya cart.');
                $scope.isSaving = false;
                $scope.importDisabled = true;
            }

            function importErrorFn() {
                Snackbar.error('Failed to import. Please try again.');
                $scope.isSaving = false;
            }
        }

        $scope.import = importCart;
    }
})();