(function() {
    'use strict';

    angular.module('groupshop.orders.controllers').
    controller('OrderNewBtnCtrl', OrderNewBtnCtrl);

    OrderNewBtnCtrl.$inject = ['$scope', 'ngDialog'];

    function OrderNewBtnCtrl($scope, ngDialog) {
        $scope.new = function () {
            ngDialog.open({
                template: 'partials/orders/edit.html',
                controller: 'OrderNewCtrl',
                className: 'ngdialog-theme-plain',
                scope: $scope,
                closeByDocument: false
            });
        };
    }
})();