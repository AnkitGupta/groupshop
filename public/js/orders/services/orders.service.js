(function() {
    angular.module('groupshop.orders.services').

    factory('Order', ['$resource', function($resource) {
       return $resource('/api/orders/:id', {id: '@id'}, {
           update: {
               method: 'PUT'
           },
           paginated_query: {
               method: 'GET',
               isArray: false
           }
       });
    }]).

    factory('AccountOrder', ['$resource', function($resource) {
       return $resource('/api/accounts/:username/orders');
    }]);
})();