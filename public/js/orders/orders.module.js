(function() {
    angular.module('tour/tour.tpl.html', []).run([
        '$templateCache',
        function ($templateCache) {
            $templateCache.put('tour/tour.tpl.html', '<div class="tour-tip groupshop-tour-tip">\n' + '    <span class="tour-arrow tt-{{ ttPlacement }}"></span>\n' + '    <div class="tour-content-wrapper">\n' + '        <p ng-bind="ttContent"></p>\n' + '        <a ng-click="setCurrentStep(getCurrentStep() + 1)" ng-bind="ttNextLabel" class="btn btn-primary"></a>\n' + '        <a ng-click="closeTour()" class="tour-close-tip">\xd7</a>\n' + '    </div>\n' + '</div>');
        }
    ]);

    angular.module('groupshop.orders', [
        'groupshop.orders.controllers',
        'groupshop.orders.directives',
        'groupshop.orders.services',
        'ngDialog',
        'angular-tour',
        'tour/tour.tpl.html'
    ]).
    constant('ORDER_CONSTANTS', {
        'OUTLETS': [
                { id: 'localbanya', name: 'Localbanya' },
                { id: 'bigbasket', name: 'BigBasket' },
                { id: 'jabong', name: 'Jabong' }
        ],
        'OUTLET_LINKS': {
            'localbanya': 'http://www.localbanya.com',
            'bigbasket': 'http://www.bigbasket.com',
            'jabong': 'http://www.jabong.com'
        },
        'STATUSES': [
                { id: 0, name: 'OPEN' },
                { id: 1, name: 'IN PROCESS' },
                { id: 2, name: 'COMPLETED' }
        ]
    });

    angular.module('groupshop.orders.controllers', []);
    angular.module('groupshop.orders.services', []);
    angular.module('groupshop.orders.directives', []);
})();