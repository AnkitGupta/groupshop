(function() {
    'use strict';

    angular.module('groupshop.routes').

    config([
        '$stateProvider',
        '$urlRouterProvider',

        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');

            $stateProvider.
            state('home', {
                url: '/',
                controller: 'IndexCtrl',
                templateUrl: 'partials/layout/index.html'
            }).
            state('register', {
                url: '/register',
                controller: 'RegisterCtrl',
                templateUrl: 'partials/authentication/register.html'
            }).
            state('login', {
                url: '/login',
                templateUrl: 'partials/authentication/login.html'
            }).
            state('orders', {
                url: '/orders',
                controller: 'OrdersIndexCtrl',
                templateUrl: 'partials/orders/index.html'
            }).
            state('orderDetail', {
                url: '/orders/:id',
                controller: 'OrderDetailCtrl',
                templateUrl: 'partials/orders/detail.html'
            }).
            state('profile', {
                url: '/profiles/:username',
                controller: 'ProfileCtrl',
                templateUrl: 'partials/profiles/index.html'
            }).
            state('passwordReset', {
                url: '/accounts/password/reset/:id',
                controller: 'PasswordResetCtrl',
                templateUrl: 'partials/authentication/password-reset.html'
            }).
            state('accountActivation', {
                url: '/accounts/activation/:id',
                controller: 'AccountActivationCtrl',
                templateUrl: 'partials/authentication/account-activation.html'
            })
        }
    ]);
})();