(function() {
    'use strict';

    angular.module('groupshop.layout.controllers').

    controller('NavBarCtrl', ['$scope', 'Authentication', function($scope, Authentication) {
        $scope.user = undefined;

        activate();

        function activate() {
            $scope.$watch(function() {
                return Authentication.isAuthenticated();
            }, function(value) {
                $scope.isAuthenticated = value;
                if ($scope.isAuthenticated) {
                    $scope.user = Authentication.getAuthenticatedAccount();
                }
            });

            $scope.logout = logout;

            function logout() {
                Authentication.logout();
            }
        }
    }]);
})();