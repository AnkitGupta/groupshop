(function() {
    'use strict';

    angular.module('groupshop.layout.controllers').
    controller('IndexCtrl', IndexCtrl);

    IndexCtrl.$inject = ['$scope', 'Authentication', '$state'];

    function IndexCtrl($scope, Authentication, $state) {
        if (Authentication.isAuthenticated()) {
            $state.go('orders');
        }
    }
})();