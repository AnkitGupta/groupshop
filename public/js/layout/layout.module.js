(function() {
    'use strict';

    angular.module('groupshop.layout', [
        'groupshop.layout.controllers'
    ]);

    angular.module('groupshop.layout.controllers', []);
})();