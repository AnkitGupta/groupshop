from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView


class PartialsView(TemplateView):
    def get_template_names(self):
        path = self.kwargs['dir']
        if self.kwargs['module']:
            path = path + '/' + self.kwargs['module']

        return [path + '/' + self.kwargs['page']]

urlpatterns = patterns('',
    url(r'^api/', include('authentication.urls')),
    url(r'^api/', include('orders.urls')),
    url(r'^api/', include('products.urls'))
)

urlpatterns += patterns('',
    # Render entry point to SPA
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    # Render angular partial/directive templates
    url(r'^(?P<dir>partials|directives)/(?P<module>[-\w.]+)/(?P<page>[-\w.]+.html)/$', PartialsView.as_view()),
    # DRF API Auth
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # Admin interface # sha1('admin.spjmr')
    url(r'^f10ede48365f7200ade299ecbda49e2e587eed0d/admin/', include(admin.site.urls)),
)