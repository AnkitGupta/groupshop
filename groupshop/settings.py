"""
Django settings for groupshop project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'z=3u1y(i3(l%k0$v6+#14!)^m@c-l%idf&1+*z9e-d$oobs5ik'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'authentication',
    'orders',
    'products',
    'post_office',
    'compressor',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'groupshop.urls'

WSGI_APPLICATION = 'groupshop.wsgi.application'

AUTH_USER_MODEL = 'authentication.Account'

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/public-files/

STATIC_URL = '/public/'

STATICFILES_DIRS = (
    'public',
)

STATIC_ROOT = 'staticfiles'

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'public/templates'),
)

EMAIL_BACKEND = 'post_office.EmailBackend'

POST_OFFICE = {
    'DEFAULT_PRIORITY': 'now'
}

# Email settings
EMAIL_USE_TLS = True
EMAIL_HOST='smtp.groupshop.in'
EMAIL_PORT=587
EMAIL_HOST_USER='admin@groupshop.in'
EMAIL_HOST_PASSWORD=')JtbLcG2'