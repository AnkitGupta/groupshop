from post_office import mail
from post_office.models import EmailTemplate


def get_or_create_template(name=None, subject=None, path=None):
    queryset = EmailTemplate.objects.filter(name=name)
    if queryset.exists():
        return queryset[0]
    else:
        return EmailTemplate.objects.create(
            name=name,
            subject=subject,
            content=open('public/templates/' + path + '.txt', 'r').read(),
            html_content=open('public/templates/' + path + '.html', 'r').read(),
        )


def send_mail(template=None, to_email=None, context=None):
    try:
        from_email = 'admin@groupshop.in'
        return mail.send(
            [to_email],
            from_email,
            template=template,
            context=context,
        )
    except ValueError:
        return False