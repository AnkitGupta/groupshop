# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20141220_1848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(default=0.0, max_digits=20, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='product',
            name='quantity',
            field=models.DecimalField(default=0.0, max_digits=20, decimal_places=2),
        ),
    ]
