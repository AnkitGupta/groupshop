# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('quantity', models.DecimalField(max_digits=20, decimal_places=2)),
                ('price', models.DecimalField(max_digits=20, decimal_places=2)),
                ('link', models.URLField()),
                ('order', models.ForeignKey(related_name=b'products', to='orders.Order')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
