from rest_framework import serializers
from products.models import Product
from orders.serializers import OrderSerializer


class ProductSerializer(serializers.ModelSerializer):
    order = OrderSerializer(exclude_fields=('group_orders',), read_only=True)
    price = serializers.DecimalField(max_digits=20, decimal_places=2, coerce_to_string=False)
    quantity = serializers.DecimalField(max_digits=20, decimal_places=2, coerce_to_string=False)

    def create(self, validated_data):
        instance = Product.objects.create(**validated_data)
        return instance

    def validate(self, attrs):
        quantity = attrs.get('quantity', 0.0)
        if not quantity > 0.0:
            raise serializers.ValidationError({
                'quantity': 'Item quantity should be greater than 0!'
            })

        return attrs

    class Meta:
        model = Product
        fields = ('id', 'order', 'price', 'quantity', 'link')