from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, views, permissions, viewsets
from products.models import Product
from products.serializers import ProductSerializer
from products.permissions import IsProductOwner

from orders.models import Order


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def perform_create(self, serializer):
        order = None
        if self.request.data.get('order'):
            order = Order.objects.get(pk=self.request.data.get('order'))
        serializer.save(order=order)

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return [permissions.IsAuthenticated()]
        return [permissions.IsAuthenticated(), IsProductOwner()]


class OrderProductList(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_all_orders(self, order, orders_all):
        for group_order in order.group_orders.all():
            orders_all.append(group_order)
            self.get_all_orders(group_order, orders_all)
        return orders_all

    def get_queryset(self):
        products_set = self.queryset.filter(order__pk=self.kwargs.get('pk'))

        if self.request.query_params.get('recursive') and self.request.query_params.get('recursive') == 'true':
            try:
                order = Order.objects.get(pk=self.kwargs.get('pk'))
                orders_all = self.get_all_orders(order, [order])
                products_set = self.queryset.filter(order__in=orders_all)
            except ObjectDoesNotExist:
                pass

        return products_set