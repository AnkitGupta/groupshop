from django.db import models
from orders.models import Order


class Product(models.Model):
    order = models.ForeignKey(Order, related_name='products')
    title = models.CharField(max_length=200, blank=True)
    quantity = models.DecimalField(max_digits=20, decimal_places=2)
    price = models.DecimalField(max_digits=20, decimal_places=2)
    link = models.URLField()

    def __unicode__(self):
        return self.title
