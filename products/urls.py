from django.conf.urls import patterns, url, include
from rest_framework import routers

from products.views import ProductViewSet, OrderProductList

router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)

urlpatterns = patterns('',
    url(r'^orders/(?P<pk>\d+)/products$', OrderProductList.as_view(), name='orderproduct-list'),
    url(r'^', include(router.urls)),
)