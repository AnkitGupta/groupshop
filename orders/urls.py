from django.conf.urls import patterns, url, include
from rest_framework import routers

from orders.views import OrderViewSet, AccountOrderList

router = routers.DefaultRouter()
router.register(r'orders', OrderViewSet)

urlpatterns = patterns('',
    url(r'^accounts/(?P<username>[0-9a-zA-Z_-]+)/orders$', AccountOrderList.as_view(), name='accountorder-list'),
    url(r'^', include(router.urls)),
)