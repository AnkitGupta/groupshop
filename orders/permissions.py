from rest_framework import permissions


class IsOwnerOfOrder(permissions.BasePermission):
    """
    Custom permission to only allow owners of an order to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if request.user:
            # Write permissions are only allowed to the owner of the Order.
            return obj.owner == request.user
        return False