from rest_framework import serializers
from orders.models import Order
from products.models import Product
from authentication.serializers import AccountSerializer


class OrderMixin(object):
    def get_total(self, order):
        total = 0
        products = Product.objects.filter(order_id=order.id)
        for product in products:
            total += product.price * product.quantity

        for group_order in order.group_orders.all():
            total += self.get_total(group_order)

        return total


class GroupOrderSerializer(serializers.ModelSerializer, OrderMixin):
    def to_representation(self, instance):
        serializer = self.parent.parent.__class__(instance, context=self.context)
        return serializer.data


class OrderSerializer(serializers.ModelSerializer, OrderMixin):
    group_orders = GroupOrderSerializer(many=True, read_only=True)
    owner = AccountSerializer(read_only=True)
    total = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        exclude_fields = kwargs.pop('exclude_fields', None)
        super(OrderSerializer, self).__init__(*args, **kwargs)

        if exclude_fields is not None:
            exclude = set(exclude_fields)
            for field_name in exclude:
                self.fields.pop(field_name)

    def create(self, validated_data):
        fields = ['group_orders', 'observers']

        many_to_many = {}
        for field_name in fields:
            if field_name in validated_data:
                many_to_many[field_name] = validated_data.pop(field_name)

        try:
            instance = Order.objects.create(**validated_data)
        except TypeError as exc:
            msg = (
                'Error occurred! %s', exc
            )
            raise TypeError(msg)

        # Save many-to-many relationships after the instance is created.
        if many_to_many:
            for field_name, value in many_to_many.items():
                setattr(instance, field_name, value)

        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return instance

    class Meta:
        model = Order
        fields = ('id', 'online_store', 'body', 'created_at', 'owner', 'status', 'group_orders', 'observers', 'total')