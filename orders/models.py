from django.db import models
from groupshop import settings


class Order(models.Model):
    online_store = models.CharField(max_length=200)
    body = models.TextField(blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='orders')
    group_orders = models.ManyToManyField('self', blank=True, null=True, symmetrical=False)
    observers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, null=True)

    # Order status
    OPEN_STATUS = 0
    IN_PROCESS_STATUS = 1
    COMPLETED_STATUS = 2
    STATUS_CHOICES = (
        (OPEN_STATUS, 'Open'),
        (IN_PROCESS_STATUS, 'In Process'),
        (COMPLETED_STATUS, 'Completed'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES, default=OPEN_STATUS)

    def __unicode__(self):
        return '{0}'.format(self.online_store)

    class Meta:
        ordering = ['-updated_at']