# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0006_remove_order_total'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='merged_orders',
        ),
        migrations.AddField(
            model_name='order',
            name='group_orders',
            field=models.ManyToManyField(related_name='group_orders_rel_+', null=True, to='orders.Order', blank=True),
            preserve_default=True,
        ),
    ]
