# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_auto_20141230_1402'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='group_orders',
            field=models.ManyToManyField(to=b'orders.Order', null=True, blank=True),
        ),
    ]
