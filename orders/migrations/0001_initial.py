# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, blank=True)),
                ('body', models.TextField(blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('status', models.IntegerField(default=1, choices=[(1, b'Open'), (1, b'In Process'), (2, b'Completed')])),
                ('merged_orders', models.ManyToManyField(related_name='merged_orders_rel_+', null=True, to='orders.Order', blank=True)),
                ('observers', models.ManyToManyField(to=settings.AUTH_USER_MODEL, null=True, blank=True)),
                ('owner', models.ForeignKey(related_name=b'orders', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-date'],
            },
            bases=(models.Model,),
        ),
    ]
