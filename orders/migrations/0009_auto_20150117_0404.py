# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0008_auto_20141231_1331'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='title',
            new_name='online_store',
        ),
    ]
