# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'ordering': ['-updated_at']},
        ),
        migrations.RenameField(
            model_name='order',
            old_name='date',
            new_name='created_at',
        ),
        migrations.AddField(
            model_name='order',
            name='updated_at',
            field=models.DateTimeField(default=datetime.date(2014, 12, 19), auto_now_add=True),
            preserve_default=False,
        ),
    ]
