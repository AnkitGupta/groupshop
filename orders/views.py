import json, requests

from django.db.models import Q
from rest_framework import generics, viewsets, permissions, views, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

from bs4 import BeautifulSoup

from orders.serializers import OrderSerializer
from orders.models import Order
from orders.permissions import IsOwnerOfOrder


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_paginate_by(self):
        if self.request.GET.get('page'):
            return 10
        return None

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return [permissions.IsAuthenticated()]
        return [permissions.IsAuthenticated(), IsOwnerOfOrder()]

    def perform_create(self, serializer):
        group_orders = []
        status = Order.OPEN_STATUS

        if self.request.DATA.get('group_orders'):
            status = Order.IN_PROCESS_STATUS
            for order_pk in self.request.DATA['group_orders']:
                group_order = Order.objects.get(pk=order_pk)
                group_order.status = Order.COMPLETED_STATUS
                group_order.save()
                group_orders.append(group_order)

        serializer.save(owner=self.request.user, group_orders=group_orders, status=status)

    def perform_update(self, serializer):
        order = Order.objects.get(pk=self.kwargs.get('pk'))
        for group_order in order.group_orders.all():
            if not group_order.group_orders.all():
                group_order.status = Order.OPEN_STATUS
            else:
                group_order.status = Order.IN_PROCESS_STATUS
            group_order.save()
            order.group_orders.remove(group_order)

        group_orders = []
        status = self.request.DATA.get('status') or Order.OPEN_STATUS

        if self.request.DATA.get('group_orders'):
            status = Order.IN_PROCESS_STATUS
            for order_pk in self.request.DATA['group_orders']:
                group_order = Order.objects.get(pk=order_pk)
                group_order.status = Order.COMPLETED_STATUS
                group_order.save()
                group_orders.append(group_order)

        serializer.save(group_orders=group_orders, status=status)

    def get_queryset(self):
        queryset = self.queryset
        if self.request.GET.get('status'):
            status = int(self.request.GET['status'])
            q = Q()

            if status & 1 << Order.OPEN_STATUS:
                q = q | Q(status=Order.OPEN_STATUS)

            if status & 1 << Order.IN_PROCESS_STATUS:
                q = q | Q(status=Order.IN_PROCESS_STATUS)

            if status & 1 << Order.COMPLETED_STATUS:
                q = q | Q(status=Order.COMPLETED_STATUS)

            queryset = queryset.filter(q)

        if self.request.GET.get('online_store'):
            queryset = queryset.filter(online_store=self.request.GET['online_store'])

        if self.kwargs.get('pk'):
            queryset = queryset.filter(pk=self.kwargs.get('pk'))

        return queryset

    def get_lb_cookies(self, request):
        gs_cookie_names = ['lbSession', 'lbCartId']
        gs_cookie_map = dict(
            lbSession='Cookie_LB-RM',
            lbCartId='Prod_LB_CartID'
        )
        lb_cookies = dict()

        for cookie_name in gs_cookie_names:
            if request.COOKIES.has_key(cookie_name):
                lb_cookies[gs_cookie_map.get(cookie_name)] = request.COOKIES.get(cookie_name)

        return lb_cookies

    @detail_route(methods=['post'])
    def export_localbanya(self, request, pk=None):
        LB_CART_URL = 'http://www.localbanya.com/cart/add'

        order = self.get_object()
        products = request.data

        lb_cookies = self.get_lb_cookies(request)

        if lb_cookies.has_key('Prod_LB_CartID'):
            success = True
            for product in products:
                response = requests.post(LB_CART_URL, data=product, cookies=lb_cookies)
                if not json.loads(response.content).get('status') == 'success':
                    success = False

            if success:
                order.status = Order.COMPLETED_STATUS
                order.save()
                serializer = OrderSerializer(instance=order)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response({
                    'error': 'Unexpected error occurred. Please try again!'
                }, status=status.HTTP_400_BAD_REQUEST)

        # User doesn't have required LB Cart cookie set.
        return Response({
            'error': 'Please visit localbanya.com before exporting products from groupshop.'
        }, status=status.HTTP_400_BAD_REQUEST)

    def get_product_url(self, name, id, weight_code, cookies):
        LB_SEARCH_URL = 'http://www.localbanya.com/home/ajax_search?term='

        response = requests.get(LB_SEARCH_URL + name, cookies=cookies)
        products = json.loads(response.content)

        for p in products[1:]:
            if p['id'] == id and p['weight_code'] == weight_code:
                return p['url']

        return None

    def parse_products(self, parsed_html, cookies):
        inps = parsed_html.findAll(attrs={'class': 'qtybox'})

        products = []
        for inp in inps:
            product_quantity = float(inp['value'])
            product_price = float(inp.find_parent('tr').find(attrs={'class': 'cartsubtotal'}).text)

            products.append({
                'quantity': product_quantity,
                'price': product_price/product_quantity,
                'link': self.get_product_url(
                    inp['data-name'],
                    inp['data-id'],
                    inp['data-weight-code'],
                    cookies
                )
            })

        return products

    @list_route()
    def import_localbanya(self, request):
        LB_CART_URL = 'http://www.localbanya.com/cart/add'

        lb_cookies = self.get_lb_cookies(request)

        if lb_cookies.has_key('Prod_LB_CartID'):
            dummy_product = {
                'prod_id': '5120',
                'weight_code': 'a',
                'quantity': '1'
            }
            response = requests.post(LB_CART_URL, data=dummy_product, cookies=lb_cookies)
            response_json = json.loads(response.content)
            products = self.parse_products(BeautifulSoup(response_json['response']), lb_cookies)
            if products:
                return Response(products, status=status.HTTP_200_OK)
        return Response(None, status=status.HTTP_400_BAD_REQUEST)


class AccountOrderList(generics.ListAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def is_participant(self, order, username):
        if order.owner.username == username:
            return True
        else:
            for group_order in order.group_orders.all():
                if self.is_participant(group_order, username):
                    return True
            return False

    def get_queryset(self):
        queryset = self.queryset

        if self.request.GET.get('status'):
            status = int(self.request.GET['status'])
            q = Q()

            if status & 1 << Order.OPEN_STATUS:
                q = q | Q(status=Order.OPEN_STATUS)

            if status & 1 << Order.IN_PROCESS_STATUS:
                q = q | Q(status=Order.IN_PROCESS_STATUS)

            if status & 1 << Order.COMPLETED_STATUS:
                q = q | Q(status=Order.COMPLETED_STATUS)

            queryset = queryset.filter(q)

        user_orders = []
        for order in queryset:
            if self.is_participant(order, self.kwargs.get('username')):
                user_orders.append(order)

        return user_orders